# Banksimulator

Compute credit cost, monthly payment, french TAEG and other credits properties
<br/> Built to compare housing bank credits proposition
<br/> Credit is subscribe for a <code>duration</code> expressed in years. Credit is paid monthly.
<br/> <code>insurance_month</code> is the credit insurance monthly payment.
<br/> <code>fees</code> is a dictionary of all the additional fees related to bank paperwork and brokers.

<pre><code>
mon_credit = Credit(
    name="MonCredit",
    borrowed_amount=200000,
    annual_rate=0.016,
    duration=15,
    date_subscription=date.today(),
    insurance_month=63.8,
    fees={
        "admin_fees":200,
        "war_fees":2149.25,
        "courtier":2500,
    }
)
</code></pre>

<code>get_recap()</code> method returns dictionary containing main properties
<pre><code>
{'name': 'MonCredit',
 'borrowed_amount': 200000,
 'duration': 15,
 'annual_rate': 0.016,
 'taeg': 0.02600716808586423,
 'monthly_payment': 1340.1997762885233,
 'interest_cost': 24902.709731934214,
 'fees_cost': 4849.25,
 'insurance_cost': 11484.0}
 </code></pre>
