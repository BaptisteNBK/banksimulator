from datetime import date
from scipy.optimize import minimize


class Credit:
    def __init__(
        self,
        name,
        borrowed_amount,
        annual_rate,
        duration,
        insurance_month,
        fees,
        date_subscription=date.today(),
    ):
        self.name = name
        self.borrowed_amount = borrowed_amount
        self.annual_rate = annual_rate
        self.duration = duration
        self.date_subscription = date_subscription
        self.insurance_month = insurance_month
        self.fees = fees
        self.nb_monthly_pay = duration * 12
        self.date_end = date_subscription.replace(
            year=date_subscription.year + duration
        )

    @property
    def monthly_rate(self):
        return (1 + self.annual_rate)**(1 / 12) - 1

    @property
    def interest_cost(self):
        return (
            self.get_monthly_pay(self.borrowed_amount) *
            self.nb_monthly_pay
            - self.borrowed_amount
        )

    @property
    def total_cost(self):
        return (
            self.interest_cost +
            sum(self.fees.values())
        )

    @property
    def total_insurance(self):
        return self.insurance_month * self.nb_monthly_pay

    @property
    def total_due(self):
        return (
            self.total_cost
            + self.borrowed_amount
            + self.total_insurance
        )

    @property
    def monthly_payment(self):
        return (
            (self.borrowed_amount + self.total_cost) / self.nb_monthly_pay
            + self.insurance_month
        )

    @property
    def taeg(self):
        f = lambda x: abs(
            self.borrowed_amount
            - sum([
                self.monthly_payment / (1 + x)**(k / 12)
                for k in range(1, self.nb_monthly_pay + 1)
            ])
        )
        return minimize(f, 0).x[0]

    def get_monthly_pay(self, amount):
        return (
            (
                amount *
                self.monthly_rate *
                (1 + self.monthly_rate)**(self.nb_monthly_pay)
            ) / (
                (1 + self.monthly_rate)**(self.nb_monthly_pay) - 1
            )
        )

    def get_recap(self):
        return {
            "name": self.name,
            "borrowed_amount": self.borrowed_amount,
            "duration": self.duration,
            "annual_rate": self.annual_rate,
            "taeg": self.taeg,
            "monthly_payment": self.monthly_payment,
            "interest_cost": self.interest_cost,
            "fees_cost": sum(self.fees.values()),
            "insurance_cost": self.total_insurance,
        }
